
"""
Calculadora
"""
def isPrimo(n):
    """
    Questao 02 - Parte I 
    Verificar se numero primo de 1 - 100
    """
    for i in range(2,100):
        if(n > 1 and n!=i and n%i==0):
            return False
    return True

print 'Quetao 02'
print '2 ',isPrimo(2)
print '3 ',isPrimo(3)
print '4 ',isPrimo(4)
print '11 ',isPrimo(11)
print '67 ',isPrimo(67)
print '68 ',isPrimo(68)

